/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.db;

import java.util.ArrayList;

/**
 *
 * @author Selvyn
 */
public class InstrumentController
{
    public  ArrayList<Instrument>   getInstruments()
    {
        ArrayList<Instrument> result = new ArrayList<>();
        
        result.add( new Instrument(1, "Astronomica"));
        result.add( new Instrument(2, "Borealis"));
        result.add( new Instrument(3, "Celestial"));
        result.add( new Instrument(4, "Deuteronic"));
        result.add( new Instrument(5, "Eclipse"));
        result.add( new Instrument(6, "Floral"));
        result.add( new Instrument(7, "Galactia"));
        result.add( new Instrument(8, "Heliosphere"));
        result.add( new Instrument(9, "Interstella"));
        result.add( new Instrument(10, "Jupiter"));
        result.add( new Instrument(11, "Koronis"));
        result.add( new Instrument(12, "Lunatic"));
        
        return result;
    }
    
}
