/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.celestial.wssh;

import com.celestial.db.Instrument;
import com.celestial.db.InstrumentController;
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Selvyn
 */
@Path("/sessions")
public class SesssionDemoPort 
{
    @Context
    // HttpServletRequest carries the session scope data
    private HttpServletRequest httpContext;
    
    @Context
    // ServletContext carries the application scope data
    private ServletContext  servletContext;
    
    @GET
    @Path("/read")
    public Response readSessionData()
    {
        String result = "<html> " + "<title>" + "hanc" + "</title>"
                + "<body><h1>" + "session demo is running..." + "</h1></body>" + "</html> ";
        HttpSession sess = httpContext.getSession( true );
        if( sess != null )
        {
            Integer sessionItem = (Integer)sess.getAttribute("session.key.data.item");
            Integer applicationItem = (Integer)servletContext.getAttribute("application.key.data.item");
            int sessionCounter, appCounter;
            if( sessionItem != null )
                sessionCounter = sessionItem;
            else
                sessionCounter = 0;
            if( applicationItem != null )
                appCounter = applicationItem;
            else
                appCounter = 0;
            
            result += "Session found/" + ++appCounter + "/" + ++sessionCounter + "/" + sess.getId();
            sess.setAttribute("session.key.data.item", new Integer(sessionCounter));
            servletContext.setAttribute("application.key.data.item", new Integer(appCounter));
        }           
        result += "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }
    
    @GET
    @Path("/boot")
    public Response bootSessionData()
    {
        String result = "<html> " + "<title>" + "hanc" + "</title>"
                + "<body><h1>" + "session demo is running..." + "</h1></body>" + "</html> ";
        HttpSession sess = httpContext.getSession( true );
        if( sess != null )
        {
            result += "Session found/" + 1 + "/" + 1 + "/" +sess.getId();
            servletContext.setAttribute("application.key.data.item", new Integer(1));
            sess.setAttribute("session.key.data.item", new Integer(1));
        }           
        result += "</h1></body>" + "</html> ";

        return Response.status(200).entity(result).build();
    }

    
    @GET
    @Path("/getinstruments")
    public Response getInstruments()
    {
        InstrumentController ic = new InstrumentController();
        
        ArrayList<Instrument> instruments = ic.getInstruments();
        
        return Response.ok(instruments, MediaType.APPLICATION_JSON_TYPE).build();
    }
}
